tools.tdep
===

![python](https://img.shields.io/badge/python-3.6--3.9-lightgrey.svg?style=flat-square)
![license](https://img.shields.io/pypi/l/son.svg?color=red&style=flat-square)
[![code style](https://img.shields.io/badge/code%20style-black-202020.svg?style=flat-square)](https://github.com/ambv/black)

These are a bunch of scripts and tools that facilitate working with the [TDEP method](http://ollehellman.github.io/).

